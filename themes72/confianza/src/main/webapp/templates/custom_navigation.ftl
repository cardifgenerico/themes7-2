
<#assign  class_public = ""  />
<#if public_theme == true>
    <#assign  class_public = "list-right"  />
</#if>

<#if themeDisplay.isSignedIn()>
    <#assign  class_public = class_public+"-signed"  />
</#if>     

<nav class="${nav_css_class} site-navigation ${class_public}" id="navigation" role="navigation">
    <ul class="navbar-nav">
    <#assign  count = 0 />
    <#list nav_items as nav_item>
        <#assign  count = count +1  />
        <#assign  nav_item_class = "item-" + count />

        <#if count == 1>
            <#assign  nav_item_class = nav_item_class + " first" />
        </#if>
        <!-- si esta seleccionado -->
        <#if nav_item.isSelected() >
            <#assign nav_item_class = nav_item_class + " selected" />
        </#if>
        <!-- si tiene hijos -->
        <#if nav_item.hasChildren() >
            <#assign nav_item_class = nav_item_class/>
        </#if>

        <!-- Si esta logeado -->
        <#if themeDisplay.isSignedIn()>
            <!-- si la navegacion tiene hijo -->
            <#if nav_item.hasChildren()>
                <!-- primer nivel -->                     
                <li class="${nav_item_class}">
                    <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="dropdown-toggle" data-toggle="dropdown">  <img src="" rel="${nav_item.iconURL()}" class="shortIcon">    <span class="collapse-icon-open pull-right"><span class="icon-flecha-abajo"></span></span>${nav_item.getName()}</a>
                    <!-- segundo nivel -->
                    <ul class="dropdown-menu row user-menu">
                        <#list nav_item.getChildren() as nav_child>
                            <li class="col-sm-12">
                                <ul>
                                    <li class="dropdown-header">
                                        <a href="${nav_child.getURL()}" {nav_child.getTarget()}><img src="" rel="${nav_child.iconURL()}" class="shortIcon sub"><span class="collapse-icon-open pull-left"></span>${nav_child.getName()}</a>
                                    </li>
                                    <#if nav_child.hasChildren()>
                                        <#list nav_child.getChildren() as nav_child_2>
                                        <li>
                                            <a href="${nav_child_2.getURL()}" ${nav_child_2.getTarget()}>${nav_child_2.getName()}
                                            </a>
                                        </li>
                                        </#list>
                                    </#if>
                                </ul>
                            </li>
                        </#list>
                    </ul>
                </li>
            <#else>
            <li class="${nav_item_class}">
                <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="nav_item_class">
                    <img src="" rel="${nav_item.iconURL()}" class="shortIcon">${nav_item.getName()}</a>
            </li>   
            </#if>                         
            <!-- si no esta logeado -->
        <#else>
            <!-- si la navegacion tiene hijo -->
            <#if nav_item.hasChildren()>
                <!-- primer nivel -->
                <li class="${nav_item_class}">
                    <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="dropdown-toggle" data-toggle="dropdown"><img src="" rel="${nav_item.iconURL()}" class="shortIcon"> <span class="collapse-icon-open pull-right"><span class="icon-flecha-abajo"></span>
                        </span>${nav_item.getName()}</a>
                    <!-- segundo nivel -->
                    <ul class="dropdown-menu row user-menu">
                        <#list nav_item.getChildren() as nav_child>
                            <li class="col-sm-12">
                                <ul>
                                    <li class="dropdown-header">
                                        <a href="${nav_child.getURL()}" ${nav_child.getTarget()}><img src="" rel="${nav_child.iconURL()}" class="shortIcon sub"><span class="collapse-icon-open pull-left"></span>${nav_child.getName()}</a>
                                    </li>
                                    <#if nav_child.hasChildren()>
                                        <#list nav_child.getChildren() as nav_child_2>
                                        <li>
                                            <a href="${nav_child_2.getURL()}" ${nav_child_2.getTarget()}>${nav_child_2.getName()}</a>
                                        </li>
                                        </#list>
                                    </#if>
                                </ul>
                            </li>
                        </#list>
                    </ul>
                </li>
            <#else>
                <li class="${nav_item_class}">
                    <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="nav_item_class"><img src="" rel="${nav_item.iconURL()}" class="shortIcon">${nav_item.getName()}</a>
                </li>
            </#if>                  
        </#if>
    </#list>  
    <#if themeDisplay.isSignedIn()>   
        <li class="login">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="collapse-icon-open pull-right">
                    <span class="icon-flecha-abajo"></span>
                </span>
                <div class="signed-user"> 
                    ${themeDisplay.getUser().getFullName()} 
                </div>
            </a>
            <ul class="dropdown-menu row user-menu">
                <li class="col-sm-12">
                    <ul>
                        <li class="dropdown-header">
                            <a href="${url_perfil}">
                                <span class="collapse-icon-open pull-left">
                                    <span class="icon-perfil"></span>
                                </span>
                                Mi perfil
                            </a>
                        </li>
                        <hr>
                        <li class="dropdown-header">
                            <a href="/c/portal/logout" >
                            <span class="collapse-icon-open pull-left">
                                <span class="icon-LOGIN"></span>
                            </span>
                            Cerrar Sesi&#243;n</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li> 
    </#if>  
    </ul>
</nav>