    <!-- incrustar un contenido web por id -->
    <#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>

	<#attempt>
	    <#assign myArticle = journalArticleLocalService.getArticleByUrlTitle(group_id, "${footer}")/>
	<#recover>
	    <#assign myArticle = ""/>
	</#attempt>
	<#if myArticle !="">
	    ${journalArticleLocalService.getArticleContent(myArticle, myArticle.getDDMTemplateKey(), "VIEW", locale, themeDisplay)}
	</#if>


