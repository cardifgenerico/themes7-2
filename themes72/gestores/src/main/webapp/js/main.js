// Propiedades por defecto
AUI().ready(
	'liferay-sign-in-modal',
	function(A) {
		var signIn = A.one('.sign-in > a');

		if (signIn && signIn.getData('redirect') !== 'true') {
			signIn.plug(Liferay.SignInModal);
		}
        $("#loader").delay(900).fadeOut("slow");
	}
);

// Fin propiedades por defecto

$(document).ready(function(){

	//valida timepicker 
	$('.timepicki').timepicki({
		'show_meridian':false,
		'min_hour_value':0,
		'max_hour_value':23
	});

	$('.timepicki').focus(function() {
		if($(this).prop('data-timepicki-tim', '')){
			$(this).parents('.form-group').removeClass('has-error');
			$(this).next(".form-validator-stack").remove();
		} 
	});

  	$('.action-next').append('<span class="icon-flecha-arriba"></span>');
  	$('.action-prev').append('<span class="icon-flecha-abajo"></span>');

	//valida datepicker
	$( '.datepicker' ).datepicker({
	  	format: 'dd/mm/yyyy',
	  	language: 'es',
	}).on('changeDate', function(e){
  		$(this).datepicker('hide');
	});

	$('.datepicker input').change(function(){
		$(this).parents('.form-group').removeClass('has-error');
		$(this).next('.form-validator-stack').remove();
	});

	setTimeout(function(){
	
		$('.timepicki-input').keyup(function(){    
	  		this.value = this.value.replace(/[^0-9\.]/g,'');
		});

		$('.select-primario').click(function(){
			if($(this).find('select').change()){
				$(this).parents('.form-group').removeClass('has-error');
				$(this).find('.form-validator-stack').remove();
			} else{
				$(this).parents('.form-group').addClass('has-error');
				$(this).find('.form-validator-stack').append();
			}
		});

	}, 1000);

	// Metodo para cambiar iconos dinamicamente
	$(".shortIcon").each(function(){
		var alt = $(this).attr("rel");	
		if( alt == ""){
			$(this).hide();
		}else{
			var rem = alt.replace("/image/layout_icon?img_id" , "/image/logo?img_id=");
			var src = $(this).attr("src", rem);
		}		
	});

	// diseño de input de tipo file
	(function() {
	  
	  'use strict';

	  $('.input-file').each(function() {
	    var $input = $(this),
	        $label = $input.next('.js-labelFile'),
	        labelVal = $label.html();
	    
	   $input.on('change', function(element) {
	      var fileName = '';
	      if (element.target.value) fileName = element.target.value.split('\\').pop();
	      fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
	   });
	  });

	})();


// Menu hamburguesa responsive
	$(function(){
	  $('.drop-box').each(function(){ 
	  	padreNav = $(this);
	  	// validar si hay navegacion
	    if ($('.nav-show').children().is("#navigation")) {
			// console.log("tiene navegación");
	    }else{
	    	padreNav.hide();
	    }	  	
	    $(padreNav).click(function(){
		    $('.btn-menu span').toggleClass('active'); 
		    if ($(".btn-menu span").hasClass("active")) {
		    	$(".nav-show").slideDown();
		    	$("#appendMob .login").removeClass("hidden-xs");
		    }else{
		    	$(".nav-show").slideUp();
		    }
	    });	    
	  });
	});

// mover notificaciones

var ravenous = function() { 

    if (window.matchMedia('(max-width: 1230px)').matches)
    {
		$("#wrapper .container-portal #navigation .navbar-nav li a:contains(' Notificaciones ')").hide();		                         
		$("#notifica").show();
    } else {
		$("#wrapper .container-portal #navigation .navbar-nav li a:contains(' Notificaciones ')").show();			                         
    	$("#notifica").hide();
    }
};
    if (window.matchMedia('(max-width: 1230px)').matches)
    {
		$("#wrapper .container-portal #navigation .navbar-nav li a:contains(' Notificaciones ')").hide();		                         
		$("#notifica").show();
    } else {
		$("#wrapper .container-portal #navigation .navbar-nav li a:contains(' Notificaciones ')").show();	                         
    	$("#notifica").hide();
    }
$(window).resize(ravenous);
ravenous();  


var hijo = $("#notifica .navbar-nav li a:contains(' Notificaciones ')").parent();
$("#notifica .navbar-nav li a:contains(' Notificaciones ')").append(hijo);	

	// Funcion scroll
	$(window).scroll(function(){
	    if ($(window).scrollTop() <= 30) {
	    	$("#wrapper").removeClass("fixed");
	    }
	    else {
	       $("#wrapper").addClass("fixed");       
	    }
	});


	//funcion de los modales
	$(".salir").click(function(){
		$(this).parents(".oculto").fadeOut();
	});

	$(".mostrar-modal").click(function(){
		$(".oculto").fadeIn();
	});

// embed profile responsive
   var ravenous = function() { 

        if (window.matchMedia('(max-width: 768px)').matches){
            $("#move").appendTo("#appendMob");          
        } else {
            $("#move").appendTo("#appendDesk");          
        }
    };
    $(window).resize(ravenous);
    ravenous();  


// Cambiar la animación del dropdown bootstrap
	$('.dropdown-toggle').click(function () {
	    $(this).next('.dropdown-menu').slideToggle(300);
	});

	$('.dropdown-toggle').focusout(function () {
	    $(this).next('.dropdown-menu').slideUp(300);
	});

//Quita texto del paginador
	$('.dataTables_wrapper .dataTables_paginate .previous, .dataTables_wrapper .dataTables_paginate .next').text('');

// Hora y fecha para esquema de color 'servicios'
	// if($('body').hasClass('servicios')){
	// 	var clock = function() { 
	// 		var dt = new Date();
	// 		document.getElementById("todayInfo").innerHTML = (("0"+dt.getDate()).slice(-2)) +"/"+ (("0"+(dt.getMonth()+1)).slice(-2)) +"/"+ (dt.getFullYear()) +" "+ (("0"+dt.getHours()).slice(-2)) +":"+ (("0"+dt.getMinutes()).slice(-2));
	// 	};
	// 	clock();

	// 	setInterval(function(){
	// 		clock();
	// 	 }, 60000);
	// }
});

function load10ader(){	
	
	AUI().on('domready',function(event) {
		console.log('Cargando ...');
		$('#loader').fadeIn();
	});
}

function unload10ader(){
	AUI().on('domready',function(event) {
		console.log('Termino ...');
		$('#loader').fadeOut();
	});
}

function viewMsgG3stor3s(msg) {
	$('#alert-modal').fadeIn();	
	$('#txt-alert').html(msg);	
}

