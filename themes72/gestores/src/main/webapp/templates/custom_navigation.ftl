<#assign  class_public = ""  />
<#assign usrRoles = user.getRoles()>
<#if public_theme == true>
    <#assign  class_public = "list-right"  />
</#if>

<#if themeDisplay.isSignedIn()>
    <#assign  class_public = class_public+"-signed"  />
</#if>

<!-- <#assign css_class = "css_class"> -->
<#if theme_display.getColorScheme().getCssClass() != "servicios">
    <!-- Asignar fecha y hora -->
    <div class="franja-dato hidden-xs">
        <div class="center">
           <!--  <a href="#" class="ayuda">ayuda</a> -->
            <!-- validar el menú -->
            <#if themeDisplay.isSignedIn()>  

            <#else>
            <!--
            <a href="${site_default_url}" class="logins">Login</a>-->
            </#if>  
        </div>      
    </div>      

    <!-- navegación del usuario -->
    <div class="container-portal">
        <#if themeDisplay.isSignedIn()>  
            <div class="drop-box">
                <div class="btn-menu">
                  <span></span>
                </div>                  
            </div>      
        <#else>

        </#if> 
        <div class="flex">
            <div class="navbar-header" id="heading">
                <a class="${logo_css_class}" href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
                    <img alt="${logo_description}" height="64" src="${site_logo}" />
                </a>
                <p class="seguros hidden-xs hidden-sm"></p>
            </div>
            <ul class="nav-user">
                  <#if themeDisplay.isSignedIn()> 
                    <!-- <li class="notificacion" id="notifications">
                      <a href="#" class="icon-notificacion dropdown-toggle" data-toggle="dropdown">
                        <span class="number">4</span>            
                      </a>
                        <ul class="dropdown-menu row notifica-user">
                            <li class="dropdown-header">
                                <a href="#">Acompañamiento reprogramado...</a>
                            </li>
                            <hr>
                        </ul>                    
                  </li>  --> 
                  <div id="appendDesk"></div>
                  <li class="login hidden-xs" id="move">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <div class="signed-user"> 
                            ${themeDisplay.getUser().getFullName()} 
                        </div>
                        <img src="${user.getPortraitURL(themeDisplay)}" class="portrait">
                        <span class="collapse-icon-open pull-right">
                            <span class="icon-flecha-abajo"></span>
                        </span>                    
                    </a>
                    <ul class="dropdown-menu row user-menu">
                        <li class="listFull">
                            <ul>
                                <li class="dropdown-header">
                                    <a href="${url_perfil}">
                                        Mi perfil
                                        <span class="icon-flecha-derecha"></span>
                                    </a>
                                </li>
                                <hr>
                                <li class="dropdown-header">
                                    <a href="${sign_out_url}">
                                        Cerrar Sesi&#243;n
                                        <span class="icon-flecha-derecha"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li> 
                </#if>                  
            </ul>
        </div>  
        <div id="notifica"></div>       
    </div>

    <!-- navegación de páginas -->
    <div class="nav-show">
        <div id="appendMob"></div>
        <nav class="${nav_css_class} site-navigation ${class_public}" id="navigation" role="navigation">
            <ul class="nav navbar-nav">
             <#if themeDisplay.isSignedIn()>
                    <#list usrRoles as usrRole>
                    <#if usrRole.getName() == "Gestor" || usrRole.getName() == "Gerente" || usrRole.getName() == "Administrator">
                    <li class="house ${usrRole.getName()}"><a href="${site_default_url}" class="home icon-home">
                    <span>Home</span></a></li>              
                    <#else>              
                    </#if> 
                </#list> 
            </#if>
            <#assign  count = 0 />
            <#list nav_items as nav_item>
                <#assign  count = count +1  />
                <#assign  nav_item_class = "item-" + count />

                <#if count == 1>
                    <#assign  nav_item_class = nav_item_class + " first" />
                </#if>
                <!-- si esta seleccionado -->
                <#if nav_item.isSelected() >
                    <#assign nav_item_class = nav_item_class + " selected" />
                </#if>
                <!-- si tiene hijos -->
                <#if nav_item.hasChildren() >
                    <#assign nav_item_class = nav_item_class/>
                </#if>

                    <!-- Si esta logeado -->
                    <#if themeDisplay.isSignedIn()>
                    <!-- si es pàgina pùblica -->              
                     <!-- si la navegacion tiene hijo -->
                        <#if nav_item.hasChildren()>
                        <!-- primer nivel -->                     
                            <li class="${nav_item_class}">
                                <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="" rel="${nav_item.iconURL()}" class="shortIcon">                      
                                    <span class="collapse-icon-open pull-right">
                                        <span class="icon-flecha-abajo"></span>
                                    </span>
                                    ${nav_item.getName()}              
                                </a>
                                <!-- segundo nivel -->
                                <ul class="dropdown-menu row user-menu">
                                    <#list nav_item.getChildren() as nav_child>
                                        <li class="col-sm-12">
                                            <ul>
                                                <li class="dropdown-header">
                                                    <a href="${nav_child.getURL()}" ${nav_child.getTarget()}>
                                                        <img src="" rel="${nav_child.iconURL()}" class="shortIcon sub">
                                                        <span class="collapse-icon-open pull-left"></span>
                                                        ${nav_child.getName()}
                                                    </a>
                                                </li>
                                                <#if nav_child.hasChildren()>
                                                    <#list nav_child.getChildren() as nav_child_2>
                                                    <li>
                                                        <a href="${nav_child_2.getURL()}" ${nav_child_2.getTarget()}>
                                                            ${nav_child_2.getName()}
                                                        </a>
                                                    </li>
                                                    </#list>
                                                </#if>
                                            </ul>
                                        </li>
                                    </#list>
                                </ul>
                            </li>
                        <#else>
                            <li class="${nav_item_class}">
                                <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="nav_item_class">
                                    <img src="" rel="${nav_item.iconURL()}" class="shortIcon">                  
                                    ${nav_item.getName()}              
                                </a>
                            </li>   
                        </#if>
                     <!-- si no esta logeado -->
                    <#else>
                        <!-- si la navegacion tiene hijo -->
                        <#if nav_item.hasChildren()>
                            <!-- primer nivel -->
                            <li class="${nav_item_class}">
                                <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="dropdown-toggle" data-toggle="dropdown"><img src="" rel="${nav_item.iconURL()}" class="shortIcon">                      
                                    <span class="collapse-icon-open pull-right"><span class="icon-flecha-abajo"></span>
                                    </span>${nav_item.getName()}</a>
                                <!-- segundo nivel -->
                                <ul class="dropdown-menu row user-menu">
                                    <#list nav_item.getChildren() as nav_child>
                                        <li class="col-sm-12">
                                            <ul>
                                                <li class="dropdown-header">
                                                    <a href="${nav_child.getURL()}" ${nav_child.getTarget()}><img src="" rel="${nav_child.iconURL()}" class="shortIcon sub"><span class="collapse-icon-open pull-left"></span>${nav_child.getName()}</a>
                                                </li>
                                                <#if nav_child.hasChildren()>
                                                    <#list nav_child.getChildren() as nav_child_2>
                                                    <li>
                                                        <a href="${nav_child_2.getURL()}" ${nav_child_2.getTarget()}>${nav_child_2.getName()}</a>
                                                    </li>
                                                    </#list>
                                                </#if>
                                            </ul>
                                        </li>
                                    </#list>
                                </ul>
                            </li>
                        <#else>
                            <li class="${nav_item_class}">
                                <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="nav_item_class"><img src="" rel="${nav_item.iconURL()}" class="shortIcon">${nav_item.getName()}</a>
                            </li>
                        </#if>                                                   
                    </#if>
            </#list>    
            </ul>
        </nav>        
    </div>
<#else>
    <div class="franja-dato">
        <div class="center">
           <!-- <p class="time"><span class="icon-hora"></span><span id="todayInfo"></span></p>  -->
        </div>      
    </div>  

    <div class="container-portal">
        <#if themeDisplay.isSignedIn()>  
            <div class="drop-box">
                <div class="btn-menu">
                  <span></span>
                </div>                  
            </div>      
        <#else>
            <div class="drop-box">
                <div class="btn-menu">
                  <span></span>
                </div>                  
            </div>  
        </#if> 
        <div class="flex">
            <div class="navbar-header" id="heading">
                <a class="${logo_css_class}" href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
                    <img alt="${logo_description}" height="64" src="${site_logo}" />
                </a>
                <p class="seguros">Ecosistema de servicios</p>
            </div>
            <ul class="nav-user">
                  <#if themeDisplay.isSignedIn()> 
                    <!-- <li class="notificacion" id="notifications">
                      <a href="#" class="icon-notificacion dropdown-toggle" data-toggle="dropdown">
                        <span class="number">4</span>            
                      </a>
                        <ul class="dropdown-menu row notifica-user">
                            <li class="dropdown-header">
                                <a href="#">Acompañamiento reprogramado...</a>
                            </li>
                            <hr>
                        </ul>                    
                  </li>  --> 
                  <div id="appendDesk"></div>
                    <li class="login hidden-xs" id="move">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <span class="icon-perfil iconoUsuario"></span>
                            <div class="signed-user">  ${themeDisplay.getUser().getFullName()}  </div>
                            <span class="collapse-icon-open pull-right">
                                <span class="icon-flecha-abajo"></span>
                            </span>                    
                        </a>
                        <ul class="dropdown-menu row user-menu">
                            <li class="listFull">
                                <ul>
                                    <li class="dropdown-header">
                                        <a href="${url_perfil}">
                                            Mi perfil
                                            <span class="icon-flecha-derecha"></span>
                                        </a>
                                    </li>
                                    <hr>
                                    <li class="dropdown-header">
                                        <a href="${sign_out_url}">
                                            Cerrar Sesi&#243;n
                                            <span class="icon-flecha-derecha"></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </#if> 
            </ul>
            <div class="nav-show">
                <div id="appendMob"></div>
                <nav class="${nav_css_class} site-navigation ${class_public}" id="navigation" role="navigation">
                    <ul class="nav navbar-nav">     
                    <#assign  count = 0 />
                    <#list nav_items as nav_item>
                        <#assign  count = count +1  />
                        <#assign  nav_item_class = "item-" + count />

                        <#if count == 1>
                            <#assign  nav_item_class = nav_item_class + " first" />
                        </#if>


                        <#if nav_item.isSelected() >
                            <#assign nav_item_class = nav_item_class + " selected" />
                        </#if>

                        <#if nav_item.hasChildren() >
                            <#assign nav_item_class = nav_item_class/>
                        </#if>

                            <#if themeDisplay.isSignedIn()>
                                <#if nav_item.hasChildren()>
                                    <li class="${nav_item_class}">
                                        <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="dropdown-toggle" data-toggle="dropdown">
                                            <img src="" rel="${nav_item.iconURL()}" class="shortIcon">                      
                                            <span class="collapse-icon-open pull-right">
                                                <span class="icon-flecha-abajo"></span>
                                            </span>
                                            ${nav_item.getName()}              
                                        </a>
                                        <ul class="dropdown-menu row user-menu">
                                            <#list nav_item.getChildren() as nav_child>
                                                <li class="col-sm-12">
                                                    <ul>
                                                        <li class="dropdown-header">
                                                            <a href="${nav_child.getURL()}" ${nav_child.getTarget()}>
                                                                <img src="" rel="${nav_child.iconURL()}" class="shortIcon sub">
                                                                <span class="collapse-icon-open pull-left"></span>
                                                                ${nav_child.getName()}
                                                            </a>
                                                        </li>
                                                        <#if nav_child.hasChildren()>
                                                            <#list nav_child.getChildren() as nav_child_2>
                                                            <li>
                                                                <a href="${nav_child_2.getURL()}" ${nav_child_2.getTarget()}>
                                                                    ${nav_child_2.getName()}
                                                                </a>
                                                            </li>
                                                            </#list>
                                                        </#if>
                                                    </ul>
                                                </li>
                                            </#list>
                                        </ul>
                                    </li>
                                <#else>
                                    <li class="${nav_item_class}">
                                        <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="nav_item_class">
                                            <img src="" rel="${nav_item.iconURL()}" class="shortIcon">                  
                                            ${nav_item.getName()}              
                                        </a>
                                    </li>   
                                </#if>
                            <#else>
                                <#if nav_item.hasChildren()>
                                    <li class="${nav_item_class}">
                                        <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="dropdown-toggle" data-toggle="dropdown"><img src="" rel="${nav_item.iconURL()}" class="shortIcon">                      
                                            <span class="collapse-icon-open pull-right"><span class="icon-flecha-abajo"></span>
                                            </span>${nav_item.getName()}</a>
                                        <ul class="dropdown-menu row user-menu">
                                            <#list nav_item.getChildren() as nav_child>
                                                <li class="col-sm-12">
                                                    <ul>
                                                        <li class="dropdown-header">
                                                            <a href="${nav_child.getURL()}" ${nav_child.getTarget()}><img src="" rel="${nav_child.iconURL()}" class="shortIcon sub"><span class="collapse-icon-open pull-left"></span>${nav_child.getName()}</a>
                                                        </li>
                                                        <#if nav_child.hasChildren()>
                                                            <#list nav_child.getChildren() as nav_child_2>
                                                            <li>
                                                                <a href="${nav_child_2.getURL()}" ${nav_child_2.getTarget()}>${nav_child_2.getName()}</a>
                                                            </li>
                                                            </#list>
                                                        </#if>
                                                    </ul>
                                                </li>
                                            </#list>
                                        </ul>
                                    </li>
                                <#else>
                                    <li class="${nav_item_class}">
                                        <a href="${nav_item.getURL()}" ${nav_item.getTarget()} class="nav_item_class"><img src="" rel="${nav_item.iconURL()}" class="shortIcon">${nav_item.getName()}</a>
                                    </li>
                                </#if>                                                   
                            </#if>
                    </#list>    
                    </ul>
                </nav>        
            </div>
        </div>  
        <div id="notifica"></div>       
    </div>
</#if>
