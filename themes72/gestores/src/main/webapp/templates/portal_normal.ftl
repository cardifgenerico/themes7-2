<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>

	<meta content="initial-scale=1.0, width=device-width, user-scalable=0, maximum-scale=1" name="viewport"/>
	${head}

	<!-- diseños de select css -->
	<link rel="stylesheet" href="${css_folder}/libs/select/bootstrap-select.min.css">
	<!-- diseño de datatimepicker css -->
	<link rel="stylesheet" href="${css_folder}/libs/timepicki/timepicki.css">
	<link rel="stylesheet" href="${css_folder}/libs/datepicker/datepicker.css">
	<link rel="stylesheet" href="${css_folder}/libs/datatable/datatables.min.css">

	<@liferay_util["include"] page=top_head_include />
</head>

<#assign css_class_c = "${css_class}">

<#assign usrRoles = user.getRoles()>
<#list usrRoles as usrRole>
     <#if usrRole.getName() == "Gestor"  || usrRole.getName() == "Gerente" || usrRole.getName() == "Gerente-Administrador"  || usrRole.getName() == "Usuario">
        <#assign css_class_c = css_class_c?replace("has-control-menu", "")>
        <#assign css_class_c = css_class_c?replace("open", "")>
    </#if> 
</#list>   

<body class="${css_class_c}">
${body}

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<#if themeDisplay.isSignedIn()>
     <#assign roles = user.getRoles() 
            showcontrolmenu = false
     />
     <#list roles as role>
        <#if role.getName() == "Administrator">
                <#assign showcontrolmenu = true />
           <#break>
        </#if>              
    </#list> 
    <#if showcontrolmenu>
        <@liferay.control_menu />
    </#if>
</#if>

<!-- imagen de usuario, poner esta propiedad en el portal.exproperties users.image.check.token=false -->


<!-- Fin portrait -->
<div id="wrapper">
	<header class="container-fluid" id="banner" role="banner">
		<!-- incluir la navegación -->
	    
	        <#include "${full_templates_path}/custom_navigation.ftl" />
	         		
	</header>
	<section class="row-fluid" id="content">
		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>

		<div id="loader" class="confirmacion">
	        <div class="modals">
	            <div class="modal-bodys">
	              <div class="content">
	                <img src="${images_folder}/gif_loaderWhite.gif" alt="Loader" class="gifLoader">
	              </div>
	            </div>
	        </div>
	    </div>

	    <div id="alert-modal" class="oculto">
		    <div class="modals">
		      <div class="modal-bodys">
		        <div class="content">
		          <div class="salir">X</div>
		          <h3 class="head-popup">Alerta</h3>
		          <p class="txt-alerts arial" id="txt-alert"></p>
		        </div>
		      </div>
		    </div>
		</div> 
	</section>	
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

</body>

	<!-- incluir el footer -->
	
	<#if '${footer}' == 'noContent'>
	<#else>
		<#include "${full_templates_path}/footer.ftl" />
	</#if>	

</html>

<!-- librerias -->

<!-- select -->
<script type="text/javascript" src="${javascript_folder}/libs/select/bootstrap-select.js"></script>
<!-- timepicki -->
 <script type="text/javascript" src="${javascript_folder}/libs/timepicki/timepicki.js"></script>
<!-- datepicker -->
<script type="text/javascript" src="${javascript_folder}/libs/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="${javascript_folder}/libs/datepicker/moment.min.js"></script>
<script src="${javascript_folder}/libs/datepicker/locales/bootstrap-datepicker.es.js" charset="UTF-8"></script>

<!-- datatable -->
<script type="text/javascript" src="${javascript_folder}/libs/datatable/datatables.min.js"></script>


