<div class="FullAndContentPage" id="main-content" role="main">
	<div class="portlet-layout row">
		<div class="portlet-column portlet-column-only col-md-12" id="column-1">
			$processor.processColumn("column-1", "portlet-column-content portlet-column-content-only")
		</div>
	</div>

	<div class="portlet-layout row">
		<div class="portlet-column portlet-column-only col-md-12 content-page" id="column-2">
			$processor.processColumn("column-2", "portlet-column-content portlet-column-content-only")
		</div>
	</div>
</div>
