// Propiedades por defecto
AUI().ready(
    'liferay-sign-in-modal',
    function (A) {
        var signIn = A.one('.sign-in > a');

        if (signIn && signIn.getData('redirect') !== 'true') {
            signIn.plug(Liferay.SignInModal);
        }
        $("#cargando-general").delay(900).fadeOut("slow");
    }
);	

// Fin propiedades por defecto

$(document).ready(function () {

    // validar si hay navegacion

    if ($("#navigation").is("nav")) {} else {
        $(".btn-menu").hide();
    }

    // Metodo para cambiar iconos dinamicamente
    $(".shortIcon").each(function () {
        var alt = $(this).attr("rel");
        if (alt == "") {
            $(this).hide();
        } else {
            var rem = alt.replace("/image/layout_icon?img_id", "/image/logo?img_id=");
            var src = $(this).attr("src", rem);
        }
    });

    // diseño de input de tipo file

    $('input[type="file"]').change(function () {
        var value = $(this).val();
        console.log(this);
        $(this).parent().children(".data").text(value);
        $(this).parent().children(".js-value").text("Se ha cargado");
    });

    $(function () {
        var bogus;
        var triple;
        $('.filupp > input[type="file"]').change(function () {
            triple = $(this).val();
            bogus = triple.split("\\");
            $(this).parent().children(".js-value").text(bogus[bogus.length - 1]);
        });
    });
    // Menu hamburguesa responsive
    $(function () {
        $('.btn-menu').click(function () {
            $('.btn-menu span').toggleClass('active');
            if ($("#wrapper .container-portal .btn-menu span").hasClass("active")) {
                $("#navigation").slideDown();
            } else {
                $("#navigation").slideUp();
            }
        });
    });

    // mover notificaciones
    $("#wrapper .container-portal #navigation .navbar-nav li a:contains('Notificaciones')").children("img").clone().prependTo("#notifica");

    var ravenous = function () {

        if (window.matchMedia('(max-width: 1230px)').matches) {
            $("#wrapper .container-portal #navigation .navbar-nav li a:contains('Notificaciones')").hide();
            $("#counts").appendTo("#notifica");
            $("#counts").removeClass("desktop");
            $("#notifica").show();
        } else {
            $("#wrapper .container-portal #navigation .navbar-nav li a:contains('Notificaciones')").show();
            $("#counts").appendTo("#wrapper .container-portal #navigation .navbar-nav li a:contains('Notificaciones')");
            $("#counts").addClass("desktop");
            $("#navigation .navbar-nav li a:contains('Notificaciones')").parent().attr("id", "padre");    $("#_com_knowarth_notifications_portlet_UserNotificationPortlet_userNotificationEventsSearchContainerSearchContainer").appendTo("#padre .user-menu");
            $("#notifica").hide();
            $("#verNoti").appendTo("#navigation #padre > .dropdown-menu");
        }
    };
    $(window).resize(ravenous);
    ravenous();
    
    $("#padre .list-group-item-field .taglib-text-icon").text("Eliminar");

    $("#padre .list-group-item-field .readNotice").click(function(){
        setTimeout(function(){
            location.reload(); 
        }, 1000);
    });
    
    var hijo = $("#notifica .navbar-nav li a:contains('Notificaciones')").parent();
    $("#notifica .navbar-nav li a:contains('Notificaciones')").append(hijo);

    $("#navigation .navbar-nav > li").click(function(){
        if($(this).hasClass("open")){  
        }else{
            $("#wrapper").addClass("fixed");
        }
    });
    //Fin notificaciones
    
    // Funcion scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() <= 30) {
            $("#wrapper").removeClass("fixed");
        } else {
            $("#wrapper").addClass("fixed");
        }
    });

    // notificaciones

    //contador
    var ntContador = $("span.eventCounts").text();
    $("#counts").text(ntContador);
    
    if(ntContador == "0" || ntContador == ""){
        $("#padre, #notifica").hide();
    }

    //funcion de los modales

    $(".salir").click(function () {
        $(".oculto").fadeOut();
    });
    $(".mostrar-modal").click(function () {
        $(".oculto").fadeIn();
    });
    $( ".modal-bodys" ).click(function() {
    	$(this).parents(".oculto").fadeOut();
	});
	$( ".content" ).click(function( event ) {
	  event.stopPropagation();
	});   
    // validar select
    //$('.selectpicker').selectpicker({});

    // validar redirect de notificaaciones en responsive
    $("#notifica").click(function(){
        var url = $("#verNoti > a").attr("href");
        location.href = url;
    });   
    
    $("#wrapper a").click(function(){
        var vals = $(this).attr("href");
        var cont = $(this).hasClass("taglib-icon");
        if (!$(this).is(".dropdown-toggle")) {
            if(vals == undefined){
            }else if(vals.length > 30 & cont == true){
            }else if(vals.length > 30){
                $("#cargando-general").delay(400).fadeIn("slow");
            }                        
        }
    });           
    
    /*redireccionar al fallar*/

    if($(".portlet-title-text").text() == "Estado"){
        location="/"
    }else{
        $("input#_com_liferay_login_web_portlet_LoginPortlet_login").val("");
    }        
});

//Activador y mensaje del modal

function modalStandar(msg) {
    $("#general-message").fadeIn().css("display","flex");  
    $('#txt-alert').html(msg);   
}
