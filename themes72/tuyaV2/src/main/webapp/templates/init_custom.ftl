<#assign show_header_search = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-header-search")) />
<#assign public_theme = getterUtil.getBoolean(themeDisplay.getThemeSetting("public-theme")) />
<#assign url_perfil = getterUtil.getString(theme_settings["url_perfil"])/>
<#assign url_notificaciones = getterUtil.getString(theme_settings["url_notificaciones"])/>
<#assign footer = getterUtil.getString(theme_settings["footer"])/>
<#assign favicon = getterUtil.getString(theme_settings["favicon"])/>
<#assign head = getterUtil.getString(theme_settings["head"])/>
<#assign body = getterUtil.getString(theme_settings["body"])/>
