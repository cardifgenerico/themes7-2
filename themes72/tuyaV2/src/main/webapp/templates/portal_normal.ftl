<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>
	<meta content="initial-scale=1.0, width=device-width, user-scalable=0, maximum-scale=1" name="viewport"/>
	<!-- Campo para acceder al head desde el tema-->
	${head}    	
	<!--favicons-->
		<!--alkosto-->
		<#if favicon == "alkosto">
			<link href="${images_folder}/alkosto.ico" class="fav hide">
		</#if>	
		<!--carulla-->
		<#if favicon == "carulla">
			<link href="${images_folder}/carulla.ico" class="fav hide">
		</#if>	
		<!--exito-->
		<#if favicon == "exito">
			<link href="${images_folder}/exito.ico" class="fav hide">
		</#if>					
	<!-- librerias -->

	<!-- diseños de select css -->
	<link rel="stylesheet"  href="${css_folder}/libs/select/bootstrap-select.css">
	<!-- diseño de datapicker css -->
	<link rel="stylesheet"  href="${css_folder}/libs/datapicker/datepicker.css">

	<@liferay_util["include"] page=top_head_include />
</head>

<#assign css_class_c = "${css_class}">

<#assign usrRoles = user.getRoles()>
<#list usrRoles as usrRole>
    <#if usrRole.getName() == "Usuario">
        <#assign css_class_c = css_class_c?replace("has-control-menu", "")>
        <#assign css_class_c = css_class_c?replace("open", "")>
    </#if> 
</#list>   

<body class="${css_class_c}">
<!--Campo para acceder al body desde el tema-->
${body}
<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<#if themeDisplay.isSignedIn()>
     <#assign roles = user.getRoles() 
            showcontrolmenu = true
     />
     <#list roles as role>
        <#if role.getName() == "Usuario">
            <#assign showcontrolmenu = false />
           	<#break>
        </#if>             
    </#list> 
    <#if showcontrolmenu>
        <@liferay.control_menu />
    </#if>
</#if>

<!-- imagen de usuario, poner esta propiedad en el portal.exproperties users.image.check.token=false -->
<img src="/image/user_male_portrait?img_id=${user.getPortraitId()}&img_id_token=" class="hide">
<!-- Fin portrait -->
<div id="wrapper">
	<header class="container-fluid" id="banner" role="banner">
		<div class="row container container-portal">
			<div class="btn-menu">
			  <span></span>
			</div>			
			<div class="flex">
				<div class="navbar-header" id="heading">
					<a class="${logo_css_class}" href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
						<img alt="${logo_description}" height="64" src="${site_logo}" />
					</a>
				</div>
			</div>	
			<#if has_navigation>
			    <#include "${full_templates_path}/custom_navigation.ftl" />
			</#if> 	
			<div id="notifica">
			    <span id="counts"></span>
			</div>
			<div id="portletNotifica" class="hide">
                <@liferay_portlet["runtime"]
                    portletName="com_knowarth_notifications_portlet_UserNotificationPortlet"
                />		
                <div id="verNoti">
                    <a href="${url_notificaciones}">Ver todas</a>	
                </div>	    
			</div>		
		</div>
	</header>
	<section class="row-fluid" id="content">
		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>
	</section>	
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

</body>

	<!-- incluir el footer -->
	
	<#if '${footer}' == 'noContent'>
	<#else>
		<#include "${full_templates_path}/footer.ftl" />
	</#if>	
    	
</html>
<!-- modal general -->
<div class="modals oculto" id="general-message">
    <div class="modal-bodys">
        <div class="content">
            <span class="icon-cerrar salir"></span>
            <h3 class="mensaje" id="txt-alert"></h3>
        </div>
    </div>
</div>

<div class="confirmacion" id="cargando-general">
    <div class="modals">
        <div class="modal-bodys">
            <div class="content">
                <img src="${images_folder}/gif_loaderWhite.gif" alt="Loader" class="gifLoader">
            </div>
        </div>
    </div>
</div>
<!-- librerias -->

<!-- select -->
<script type="text/javascript" src="${javascript_folder}/libs/select/bootstrap-select.js"></script>
<!-- datapicker -->
<script type="text/javascript"  src="${javascript_folder}/libs/datapicker/bootstrap-datepicker.js"></script>

